<?php

/**
 * @file imagefield_cycle.common.inc
 * This file contains functions common to the module and install file
 */

/**
 * Dependency checker. This defines the JQuery Cycle path and a value to
 * determine whether or not it is present.
 */
function imagefield_cycle_dependency_check() {
  static $result = NULL;

  if (isset($result)) {
    return $result;
  }

  $path = module_invoke('libraries', 'get_path', 'jquery.cycle');
  $filepath = $path . '/jquery.cycle.all.min.js';

  $result = array(
    'path' => $path,
    'filepath' => $filepath,
    'exists' => (is_dir($path) && is_file($filepath)),
  );

  return $result;
}


/**
 * Return the version of JQuery Cycle that is installed
 */
function imagefield_cycle_get_jquery_cycle_version($filepath) {
  $version = 0;
  $pattern = '# * Version: ([0-9\.]+) \((.+)\)#';

  $js_contents = file_get_contents($filepath);
  if (preg_match($pattern, $js_contents, $matches)) {
    $version = $matches[1];
  }

  return $version;
}
