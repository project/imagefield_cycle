<?php

/**
 * @file imagefield_cycle.admin.inc
 */


/**
 * Admin overview page callback - summarizes profiles
 */
function imagefield_cycle_admin_overview() {
  // Load the common include for the dependency check
  module_load_include('inc', 'imagefield_cycle', 'imagefield_cycle.common');

  if ( ($check = imagefield_cycle_dependency_check()) && !$check['exists']) {
    drupal_set_message(t(
      'The JQuery Cycle library is not preset. Please install it into: %path',
      array(
        '%path' => $check['path'],
      )
    ), 'error');
  }

  $settings = imagefield_cycle_get_settings();

  $headers = array(t('Profile Name'), t('Settings'), t('Storage'), t('Ops'));
  $rows = array();

  foreach ($settings as $profile) {
    switch ($profile['storage']) {
      default:
      case IMAGEFIELD_CYCLE_STORAGE_NORMAL :
        $storage = t('Normal');
        $ops = array(
          l(t('Edit'), 'admin/settings/imagefield-cycle/'. $profile['name'] .'/edit'),
          l(t('Delete'), 'admin/settings/imagefield-cycle/'. $profile['name'] .'/delete'),
        );
        break;

      case IMAGEFIELD_CYCLE_STORAGE_DEFAULT :
        $storage = t('Default');
        $ops = array(
          l(t('Override'), 'admin/settings/imagefield-cycle/'. $profile['name'] .'/edit'),
        );
        break;

      case IMAGEFIELD_CYCLE_STORAGE_OVERRIDE :
        $storage = t('Overridden');
        $ops = array(
          l(t('Edit'), 'admin/settings/imagefield-cycle/'. $profile['name'] .'/edit'),
          l(t('Revert'), 'admin/settings/imagefield-cycle/'. $profile['name'] .'/revert'),
        );
        break;
    }

    $settings_string = t('%thumb -> %large', array(
      '%thumb' => $profile['thumbnail_imagecache_preset'],
      '%large' => $profile['large_imagecache_preset'],
    ));

    $rows[] = array(
      check_plain($profile['name']),
      $settings_string,
      $storage,
      implode(' | ', $ops),
    );
  }

  return theme('table', $headers, $rows);
}


/**
 * Profile add/edit form
 */
function imagefield_cycle_admin_profile_form(&$form_state, $profile = NULL) {
  $form = array();

  $form['profile'] = array(
    '#tree' => TRUE,
  );

  if (isset($profile['name'])) {
    $form['#original_profile'] = $profile;
  }

  $form['profile']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Profile Name'),
    '#description' => t('Enter the name for this profile. Only use lowercase alphanumeric values and hyphens.'),
    '#default_value' => isset($profile['name']) ? $profile['name'] : '',
  );

  // Non-normal items should be read only (the key is 'fixed')
  if ((isset($profile['storage']) && $profile['storage'] != IMAGEFIELD_CYCLE_STORAGE_NORMAL)) {
    $form['profile']['name']['#type'] = 'item';
    $form['profile']['name']['#value'] = $form['profile']['name']['#default_value'];
  }

  $presets = array();
  foreach (imagecache_presets() as $preset) {
    $presets[$preset['presetname']] = $preset['presetname'];
  }

  $form['profile']['large_imagecache_preset'] = array(
    '#type' => 'select',
    '#title' => t('Large Image Preset'),
    '#description' => t('Select the ImageCache preset to use for the large image'),
    '#options' => $presets,
    '#default_value' => isset($profile['large_imagecache_preset']) ? $profile['large_imagecache_preset'] : '',
  );

  $form['profile']['thumbnail_imagecache_preset'] = array(
    '#type' => 'select',
    '#title' => t('Thumbnail Image Preset'),
    '#description' => t('Select the ImageCache preset to use for the thumbnail images'),
    '#options' => $presets,
    '#default_value' => isset($profile['thumbnail_imagecache_preset']) ? $profile['thumbnail_imagecache_preset'] : '',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#validate' => array(''), // Skip validation on cancel
    '#submit' => array('imagefield_cycle_admin_profile_form_cancel'),
  );

  return $form;
}


/**
 * Validation handler for imagefield_cycle_admin_profile_form().
 */
function imagefield_cycle_admin_profile_form_validate(&$form, &$form_state) {
  // Ensure the name is set, if the field is available
  // The field will NOT be available for overridden profiles
  if (isset($form_state['values']['profile']['name']) && empty($form_state['values']['profile']['name'])) {
    form_set_error('profile][name', t('Profile Names are required'));
  }
  // If it is available, check it only contains valid characters
  elseif (preg_match('|[^a-z0-9\-]|', $form_state['values']['profile']['name'])) {
    form_set_error('profile][name', t('Profile Name must only contain lowercase alphanumeric and hyphens'));
  }
}


/**
 * Submit handler for imagefield_cycle_admin_profile_form().
 */
function imagefield_cycle_admin_profile_form_submit(&$form, &$form_state) {
  // Get the profile form the form
  $profile = $form_state['values']['profile'];

  if (isset($form['#original_profile'])) {
    // if we're on non-normal storage, then we need to ensure we're using the original profile name
    if ($form['#original_profile'] != IMAGEFIELD_CYCLE_STORAGE_NORMAL) {
      $profile['name'] = $form['#original_profile']['name'];
    }

    // Remove any old settings - this stops dupes when you rename
    db_query("DELETE FROM {imagefield_cycle_profiles} WHERE name = '%s'", $form['#original_profile']['name']);

    // Unset the old profile - we dont need to save it
    unset($original_profile);
  }

  // Write the settings
  drupal_write_record('imagefield_cycle_profiles', $profile);

  // Flush the static cache - just in case
  imagefield_cycle_get_settings(NULL, TRUE);

  // Redirect
  $form_state['redirect'] = 'admin/settings/imagefield-cycle';
}


/**
 * Submit handler for imagefield_cycle_admin_profile_form().
 * This one handles the cancel button
 */
function imagefield_cycle_admin_profile_form_cancel(&$form, &$form_state) {
  // Just redirect - no changes
  $form_state['redirect'] = 'admin/settings/imagefield-cycle';
}


/**
 * Profile delete confirmation form
 */
function imagefield_cycle_admin_profile_delete_confirm(&$form_state, $profile) {
  $form = array();

  // Store the profile in the form for later reference
  $form['#profile'] = $profile;

  return confirm_form(
    $form,
    t('Are you sure you want to delete the profile %name', array('%name' => $profile['name'])),
    'admin/settings/imagefield-cycle',
    t('Any output associated with this profile will no longer work.'),
    t('Delete'),
    t('Cancel'),
    'imagefield_cycle_admin_profile_delete_confirm'
  );
}


/**
 * Submit handler for imagefield_cycle_admin_profile_delete_confirm().
 */
function imagefield_cycle_admin_profile_delete_confirm_submit(&$form, &$form_state) {
  // Get the profile
  $profile = $form['#profile'];

  // Delete the stored profile settings
  db_query("DELETE FROM {imagefield_cycle_profiles} WHERE name = '%s'", $profile['name']);

  // Flush the static cache - just in case
  imagefield_cycle_get_settings(NULL, TRUE);

  // Redirect to the overview
  $form_state['redirect'] = 'admin/settings/imagefield-cycle';
}


/**
 * Profile Revert form
 * This is essentially identical to the delete form - but has different wording...
 */
function imagefield_cycle_admin_profile_revert_confirm(&$form_state, $profile) {
  $form = array();
  $form['#profile'] = $profile;
  return confirm_form(
    $form,
    t('Are you sure you want to revert the profile %name', array('%name' => $profile['name'])),
    'admin/settings/imagefield-cycle',
    t('Any output associated with this profile will return to using the profile defined by the module providing it.'),
    t('Revert'),
    t('Cancel'),
    'imagefield_cycle_admin_profile_revert_confirm'
  );
}


/**
 * Submit handler for imagefield_cycle_admin_profile_revert_confirm().
 * This just deletes the overriding entry using the delete handler
 */
function imagefield_cycle_admin_profile_revert_confirm_submit(&$form, &$form_state) {
  // Reverting IS deleteing... call the delete handler
  imagefield_cycle_admin_profile_delete_confirm_submit($form, $form_state);
}
