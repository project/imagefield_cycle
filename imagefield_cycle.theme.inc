<?php

/**
 * @file imagefield_cycle.theme.inc
 * This has the theme function for the formatter
 */

// TODO: This would PROBABLY be better served by a preprocess handler and a
//       template file


/**
 * Theme handler for the default/base formatter
 */
function theme_imagefield_cycle_formatter_default($element) {
  // Load the common include for the dependency check
  module_load_include('inc', 'imagefield_cycle', 'imagefield_cycle.common');

  // Get the JQuery Cycle path. Fail/abort if not present
  if ( ($jquery_cycle_path = imagefield_cycle_dependency_check()) && !$jquery_cycle_path['exists']) {
    drupal_set_message(t(
      'The JQuery Cycle library is not preset. Please install it into: %path',
      array(
        '%path' => $jquery_cycle_path['path'],
      )
    ), 'error');
    return '';
  }

  // Add JQuery Cycle
  drupal_add_js($jquery_cycle_path['filepath']);

  // Add the module JS - this JS does the magic pagenation
  $module_path = drupal_get_path('module', 'imagefield_cycle');
  drupal_add_js($module_path .'/imagefield_cycle.js');

  // Add the theme CSS file - this does MINIMAL theming
  drupal_add_css($module_path .'/imagefield_cycle.theme.css');

  // Extract the profile name from the formatter, ie. drop _default off the end
  list($profile_name,) = explode('_', $element['#formatter']);

  // Load the settings for this name
  $profile = imagefield_cycle_get_settings($profile_name);

  // If the profile is not availabe (returns FALSE), then go no further
  if (!$profile) {
    return;
  }

  // Init the two 'containers'
  $large_output = '';
  $thumb_output = '';

  // For each item in this field...
  foreach (element_children($element) as $key) {
    // ... Get the 'file'
    $file = $element[$key]['#item'];

    // Render a large and a thumb image into the container
    $large_output .= '<div class="large-image-item large-image-item-'. $key .'">'. theme('imagecache', $profile['large_imagecache_preset'],     $file['filepath'], $file['data']['alt'], $file['data']['title']) .'</div>';
    $thumb_output .= '<div class="thumb-image-item thumb-image-item-'. $key .'">'. theme('imagecache', $profile['thumbnail_imagecache_preset'], $file['filepath'], $file['data']['alt'], $file['data']['title']) .'</div>';
  }

  // Return a wrapper with the containers also in their own wrappers
  return '<div class="imagefield-cycle-wrapper"><div class="large-images">'. $large_output .'</div><div class="thumb-images">'. $thumb_output .'</div></div>';
}
