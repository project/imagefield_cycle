Drupal.behaviors.imagefieldCycle = function(context) {
  // TODO: Need to test what happens if there is more than one rotator on a page...
  // Should probably loop over each and use some kind of ID?!
  $('div.imagefield-cycle-wrapper div.large-images', context).cycle({
    speed: 'fast',
    cleartypeNoBg: true,
    timeout: 0,
    pager: 'div.imagefield-cycle-wrapper div.thumb-images',
    pagerAnchorBuilder: function (idx, slide) {
      return 'div.imagefield-cycle-wrapper div.thumb-images div.thumb-image-item:eq(' + idx + ')';
    }
  });
}
