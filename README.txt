-- SUMMARY --

ImageField Cycle provides a per-field JQuery Cycle handler for ImageFields.
This can be useful if you can upload several images into a single field,
but are limited on the space that the images should be displayed in.

Currently this module only supports using JQuery Cycle to display images in a
gallery-like way (a single large image with other images as thumbnail pagers
which, on click, cycle the large image)

Images are handled using ImageCache


-- REQUIREMENTS --

 * JQuery Cycle (obviously)
 * Libraries
 * ImageCache
 * ImageAPI + either ImageAPI GD or ImageAPI ImageMagick (preferable)
 * ImageField


-- INSTALLATION --

1) Go to sites/all

2) Make a libraries directory, if it doesn't already exist:
   $ mkdir libraries

3) Go into the libraries directory and make the JQuery Cycle directory
   $ cd libraries
   $ mkdir jquery.cycle

4) Download and extract JQuery Cycle into this folder. Visit The following URL
   for the latest version:
   http://jquery.malsup.com/cycle/download.html

   $ cd jquery.cycle
   $ wget http://jquery.malsup.com/cycle/release/jquery.cycle.zip?v2.99
   $ unzpip jquery.cycle.zip\?v2.99

5) Housekeeping: Remove the zip file and the __MACOSX folder, they are no
   longer needed:

   $ rm -rf jquery.cycle.zip\?v2.99 __MAXOSX/


-- CONTACT --

Nicholas Thompson (nicholasThompson) - http://drupal.org/user/59351




This project has been sponsored by :
 * iO1
   The leading Drupal Consultancy in the UK and Europe.
   http://www.io1.biz/
